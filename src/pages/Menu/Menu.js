import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

import Tabbar from '../../components/Tabbar/Tabbar';
import { isAuthenticated } from '../../components/Util/Auth';

import './Menu.css';
import '../../components/Css/App.css';

if(isAuthenticated()){ 
    const user_info = JSON.parse(localStorage.getItem('user_info'));
    var user_name = user_info.name;
    var user_identity_document = user_info.identity_document;
}


class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logout: false,
            cancel: false
        }
    }
    logoutConfirm() {
        this.setState({ logout: true })
    };
    logout() {
        this.setState({ logout: false })
        const { history } = this.props;
        history.push('/menu');
    };
    cancelClick() {
        localStorage.removeItem('user_info');
        this.setState({ logout: false })
        const { history } = this.props;
        history.push('/');
    }
    
    render() {
        return (
            <div>
                <Tabbar />
                <SweetAlert
                    show={this.state.logout}
                    text="Desejar Realmente <strong>Sair</strong> da sua conta?"
                    type="warning"
                    title=""
                    html
                    onConfirm={() => this.logout()}
                    confirmButtonText="Não"
                    showCancelButton={true}
                    cancelButtonText="Sim"
                    cancelBtnBsStyle="danger"
                    onCancel={() => this.cancelClick()} />

                <div className="container-fluid">
                    {isAuthenticated() ?
                        <Link className="row row-profile" to="/profile">
                            <div className="col-2 avatar item">
                                <i className="fas fa-user-tie bg-violet"></i>
                            </div>
                            <div className="col-8 item">
                                <h5>{user_name}</h5>
                                <h6>{user_identity_document}</h6>
                            </div>
                            <div className="col-2 item chevron">
                                <i className="fas fa-chevron-right" style={{marginTop: '12px'}}></i>
                            </div>
                        </Link>
                    : null}

                    <div className="menu-box">
                        <Link className="row row-menu" to="/">
                            <div className="col-2 avatar item">
                                <i className="fas fa-glass-cheers bg-orange"></i>
                            </div>
                            <div className="col-8 item">
                                <span className="title">Eventos</span>
                            </div>
                            <div className="col-2 item chevron">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </Link>

                    {isAuthenticated() ?
                        <Link className="row row-menu" to="/my-ticket">
                            <div className="col-2 avatar item">
                                <i className="fas fa-ticket-alt bg-lightblue"></i>
                            </div>
                            <div className="col-8 item">
                                <span className="title">Meus Ingressos</span>
                            </div>
                            <div className="col-2 item chevron">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </Link>
                    : null}
                    
                    {isAuthenticated() ?
                        <Link className="row row-menu" to="/my-ticket">
                            <div className="col-2 avatar item">
                                <i className="fas fa-shopping-cart bg-green"></i>
                            </div>
                            <div className="col-8 item no-border">
                                <span className="title">Meus Pedidos</span>
                            </div>
                            <div className="col-2 item chevron no-border">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </Link>
                    : null}
                </div>

                <div className="menu-box">                    
                    {isAuthenticated() ?
                        <Link className="row row-menu" to="/my-ticket">
                            <div className="col-2 avatar item">
                                <i className="fas fa-headset bg-silver"></i>
                            </div>
                            <div className="col-8 item">
                                <span className="title">Suporte</span>
                            </div>
                            <div className="col-2 item chevron">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </Link>
                    : null}
                    
                    <Link className="row row-menu" to="/faq">
                        <div className="col-2 avatar item">
                            <i className="fas fa-question-circle bg-purple"></i>
                        </div>
                        <div className="col-8 item">
                            <span className="title">FAQ</span>
                        </div>
                        <div className="col-2 item chevron">
                            <i className="fas fa-chevron-right"></i>
                        </div>
                    </Link>
                    
                    <Link className="row row-menu" to="/contact">
                        <div className="col-2 avatar item">
                            <i className="far fa-envelope bg-gold"></i>
                        </div>
                        <div className="col-8 item">
                            <span className="title">Contato</span>
                        </div>
                        <div className="col-2 item chevron">
                            <i className="fas fa-chevron-right"></i>
                        </div>
                    </Link>

                    {isAuthenticated() ?
                        <div className="row row-menu" onClick={() => this.logoutConfirm()}>
                            <div className="col-2 avatar item">
                                <i className="fas fa-sign-out-alt bg-red"></i>
                            </div>
                            <div className="col-8 item no-border">
                                <span className="title">Sair</span>
                            </div>
                            <div className="col-2 item chevron no-border">
                                <i className="fas fa-chevron-right"></i>
                            </div>
                        </div>
                    : null}

                    {!isAuthenticated() ?
                    <Link className="row row-menu" to="/login">
                        <div className="col-2 avatar item">
                            <i className="fas fa-sign-in-alt bg-red"></i>
                        </div>
                        <div className="col-8 item no-border">
                            <span className="title">Entrar</span>
                        </div>
                        <div className="col-2 item chevron no-border">
                            <i className="fas fa-chevron-right"></i>
                        </div>
                    </Link>
                    : null}
                </div>
                    
                    <div className="row box-menu">
                        {/* <Link className="menu-item col-12" to="/">
                            <div className="col-menu">
                                <i className="fas fa-glass-cheers"></i>
                                <span className="title">Eventos</span>
                            </div>
                        </Link> */}

                        {/* {isAuthenticated() ?
                            <Link className="menu-item col-12" to="/my-tickets">
                                <div className="col-menu">
                                    <i className="fas fa-ticket-alt"></i>
                                    <span className="title">Meus Ingressos</span>
                                </div>
                            </Link>
                            : null} */}

                        {/* {isAuthenticated() ?
                            <Link className="menu-item col-12" to="/">
                                <div className="col-menu">
                                    <i className="fas fa-shopping-cart"></i>
                                    <span className="title">Meus Pedidos</span>
                                </div>
                            </Link>
                            : null} */}

                        {/* {isAuthenticated() ?
                            <Link className="menu-item col-12" to="/">
                                <div className="col-menu">
                                    <i className="fas fa-headset"></i>
                                    <span className="title">Suporte</span>
                                </div>
                            </Link>
                            : null} */}

                        {/* {isAuthenticated() ?
                            <Link className="menu-item col-12" to="/">
                                <div className="col-menu">
                                    <i className="fas fa-user-tie"></i>
                                    <span className="title">Meu Perfil</span>
                                </div>
                            </Link>
                            : null} */}

                        {/* <Link className="menu-item col-12" to="/faq">
                            <div className="col-menu">
                                <i className="fas fa-question-circle"></i>
                                <span className="title">FAQ</span>
                            </div>
                        </Link> */}

                        {/* <Link className="menu-item col-12" to="/contact">
                            <div className="col-menu">
                                <i className="far fa-envelope"></i>
                                <span className="title">Contato</span>
                            </div>
                        </Link>

                        {!isAuthenticated() ?
                            <Link className="menu-item col-12" to="/login">
                                <div className="col-menu">
                                    <i className="fas fa-sign-in-alt"></i>
                                    <span className="title">Entrar</span>
                                </div>
                            </Link>
                            : null}

                        {isAuthenticated() ?
                            <div className="menu-item col-12" onClick={() => this.logoutConfirm()}>
                                <div className="col-menu">
                                    <i className="fas fa-sign-out-alt"></i>
                                    <span className="title">Sair</span>
                                </div>
                            </div>
                            : null} */}

                    </div>
                </div>
            </div>
        )
    }
}

export default Menu;