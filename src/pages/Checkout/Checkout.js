import React, { Component } from 'react';
import { isAuthenticated } from '../../components/Util/Auth';
import api from '../../components/Util/api';
import { Link } from 'react-router-dom';
import Header from '../../components/Header/Header';
import MaskedInput from 'react-text-mask';
import { Formik } from 'formik';
import * as Yup from 'yup';

import Card from './Card';

import './Checkout.css';
import '../../components/Css/App.css';
import flag_cards from '../../images/flag_cards.png';

//MASKS
const dddNumberMask = [
    "(", /[1-9]/, /\d/, ")"
];
const phoneNumberMask = [
    /\d/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/
];
const cpfNumberMask = [
    /\d/, /\d/, /\d/, ".", /\d/, /\d/, /\d/, ".", /\d/, /\d/, /\d/, "-", /\d/, /\d/
];
const dataMask = [
    /\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/
];
const zipcodeMask = [
    /\d/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/
];
const cardNumber = [
    /\d/, /\d/, /\d/, /\d/, " ", /\d/, /\d/, /\d/, /\d/, " ", /\d/, /\d/, /\d/, /\d/, " ", /\d/, /\d/, /\d/, /\d/
];
//MASKS

const separator = '/';

class Checkout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            events: null,
            event_name: null,
            priceTotal: null,
            totalInterest: '',
            lot: [],
            type: '',
            user_name: '',
            user_identity_document: '',
            auth_token: '',
            showName: false,
            userName: '',
            parcelamento_permitido: 0,
            ar_installments: [],

            zipcode: '',
            estado: 'Selecione o Estado',
            states: [],
            cities: [],
            adddress: {},
            city: '',
            complement: '',
            identity_document: '',
            email: '',
            name: '',
            cell_phone: '',

            //VALIDATE
            validate_zipcode: false,
            validate_street: false,
            validate_number: false,
            validate_neighborhood: false,
            validate_uf: false,
            validate_city: false,
            noZipcode: false,
            isCPF: '',
            showWarning: false,
            //VALIDATE
        }

        this.CheckZipcode = this.CheckZipcode.bind(this);
        this.onChangeInstallments = this.onChangeInstallments.bind(this);
    }

    async componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const {
            events,
            events: { tickets },
            total
        } = cart;
        
        this.setState({ 
            cart: cart,
            totalInterest: cart.total.price.toFixed(2).replace('.', ','),
            events,
            event_name: events.name,
            priceTotal: cart.total.price
        });

        if (isAuthenticated()) {
            const user_info = JSON.parse(localStorage.getItem('user_info'));
            this.setState({ user_name: user_info.name })
            this.setState({ user_identity_document: user_info.identity_document })
            this.setState({ auth_token: user_info.auth_token });

            const company_info = JSON.parse(localStorage.getItem('company_info'));
            this.setState({ parcelamento_permitido: company_info.parcelamento_permitido });
        }

        //TERMOS DE USO
        await api.get('terms')
        .then(res => {
            var terms = JSON.stringify(res.data)
            this.setState({terms: terms})
        })
        .catch(error => {
            alert('Termos de uso não encontrado');
        });
        //TERMOS DE USO

        //ESTADOS
        await api.get('address/states')
        .then(res => {
            const states = res.data.data;
            this.setState({ states });
        })
        //ESTADOS
        
        //PARCELAS
        await api.get(`orders/rate/12${this.state.parcelamento_permitido}`)
        .then(res => {
            let ar_installments = [];
            const installments = res.data.data;
            installments.forEach(value => {
                var rate = (this.state.priceTotal * value.rate) / value.installment;
                ar_installments.push(rate.toFixed(2).replace('.', ','))
            });
            this.setState({ ar_installments: ar_installments });
        })
        //PARCELAS
    }

    //CALCULANDO PARCELAS COM JUROS
    onChangeInstallments(e){
        const [value, e_installment] = e.target.value.split(separator);
        let installment = parseInt(e_installment) + 1;
        installment > 1 ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
        const priceTotal = value.replace(',', '.') * parseInt(installment);
        this.setState({ totalInterest: priceTotal.toFixed(2).replace('.', ',') });
    }
    //CALCULANDO PARCELAS COM JUROS

    //CEP
    CheckZipcode() {
        let cep = this.state.zipcode.substring(0, 9);
        api.post('address/zipcode', {cep})
            .then(res => {
                const address = res.data.data;
                this.setState({ noZipcode: false })
                this.setState({ address: address })
                this.setState({ neighborhood: address.bairro });
                this.setState({ city: address.cidade });
                this.setState({ complement: address.complemento });
                this.setState({ street: address.endereco });
                this.setState({ uf: address.estado });
                this.setState({ id_city: address.id_cidade });
                if (address) {
                    api.get(`address/cities/${this.state.uf}`)
                        .then(res => {
                            const cities = res.data.data;
                            this.setState({ cities });
                        })
                }
            }).catch(error => {
                this.setState({ noZipcode: true })
                console.log(error)
            });
    }
    //CEP

    //GET CITIES
    GetCities = (event) => {
        api.get(`address/cities/${event.target.value}`)
            .then(res => {
                const cities = res.data.data;
                this.setState({ cities });
            })
    }
    //GET CITIES

    //VALIDAR CPF
    ValidaCPF() {
        let strCPF = this.state.cpf;
        let cpf = strCPF.replace(/[^\d]+/g,'');	
        console.log(cpf);
        if(cpf === '') {
            this.setState({ isCPF: false });
        }
        // Elimina CPFs invalidos conhecidos	
        if (cpf.length !== 11 || cpf === "00000000000" || cpf === "11111111111" || cpf === "22222222222" || cpf === "33333333333" || cpf === "44444444444" || cpf === "55555555555" || cpf === "66666666666" || cpf === "77777777777" || cpf === "88888888888" || cpf === "99999999999") {
                this.setState({ isCPF: false });
            }		
        // Valida 1o digito	
        let add = 0;	
        for (let i = 0; i < 9; i++)		
            add += parseInt(cpf.charAt(i)) * (10 - i);	
            let rev = 11 - (add % 11);	
            if (rev === 10 || rev === 11)		
                rev = 0;	
            if (rev !== parseInt(cpf.charAt(9)))	{
                this.setState({ isCPF: false });
            }		
        // Valida 2o digito	
        add = 0;	
        for (let i = 0; i < 10; i ++)		
            add += parseInt(cpf.charAt(i)) * (11 - i);	
        rev = 11 - (add % 11);	
        if (rev === 10 || rev === 11)	
            rev = 0;	
        if (rev !== parseInt(cpf.charAt(10))) {
            this.setState({ isCPF: false });
        }else {
            this.setState({ isCPF: true });
        }
    }
    //VALIDAR CPF

    render() {
        return (
            <div>
                <Header Title={this.state.event_name} ToPage="/" />
                <div className="container-fluid padding-15 event">
                    <h4 className="mt-5 text-center">Resumo do Pedido</h4>
                    <hr />
                    <Card />

                    <div className="row">
                        <div className='col text-center'>
                            <h4 className="mt-2 text-center">Pagamento</h4>
                            <img src={flag_cards} alt=""/>
                            <hr />
                        </div>
                    </div>

                    <Formik
                        initialValues={{ card_number: '', name: '', cvc: '', ddd: '', cell_phone: '', birth_date: '', identity_document: '', terms: false, save_card: false }} npm audo
                        onSubmit={(values, { setSubmitting }) => {
                            console.log('teste');
                            //VALIDATE ZIPCODE
                            if (values.zipcode === undefined) {
                                this.setState({ validate_zipcode: true });
                            } else {
                                this.setState({ validate_zipcode: false });
                                this.setState({ zipcode: values.zipcode });
                            }
                            //VALIDATE ZIPCODE

                            //VALIDATE STREET
                            if (values.street === undefined) {
                                this.setState({ validate_street: true });
                            }
                            if (this.state.street !== '') {
                                this.setState({ validate_street: false });
                            }
                            if (this.state.street === '' && values.street !== undefined) {
                                this.setState({ street: values.street });
                            }
                            //VALIDATE STREET

                            //VALIDATE NUMBER
                            if (values.number === undefined) {
                                this.setState({ validate_number: true });
                            } else {
                                this.setState({ validate_number: false });
                                this.setState({ number: values.number });
                            }
                            //VALIDATE NUMBER

                            //COMPLEMENT
                            this.setState({ complement: values.complement });
                            //COMPLEMENT

                            //VALIDATE NEIGHBORHOOD
                            if (values.neighborhood === undefined) {
                                this.setState({ validate_neighborhood: true });
                            }
                            if (this.state.neighborhood !== '') {
                                this.setState({ validate_neighborhood: false });
                            }
                            if (this.state.neighborhood === '' && values.neighborhood !== undefined) {
                                this.setState({ neighborhood: values.neighborhood });
                            };
                            //VALIDATE NEIGHBORHOOD

                            //VALIDATE UF
                            if (this.state.uf === '') {
                                this.setState({ validate_uf: true });
                            } else {
                                this.setState({ validate_uf: false });
                            }
                            //VALIDATE UF

                            //VALIDATE CITY
                            if (this.state.city === '') {
                                this.setState({ validate_city: true });
                            } else {
                                this.setState({ validate_city: false });
                            }
                            //VALIDATE CITY

                            let number = values.number;
                            let name = values.name;
                            let cell_phone = values.ddd + ' ' + values.cell_phone;
                            let identity_document = values.identity_document;
                            // api.post('users/register', { name, cell_phone, identity_document, email, password })
                            //     .then(res => {
                            //         console.log(res);
                            //         let data = JSON.stringify(res.data)
                            //         localStorage.setItem('user_info', data);
                            //         this.setState({ register: true })
                            //         const { history } = this.props
                            //         history.push('/')
                            //     })
                            //     .catch(error => {
                            //         this.setState({ noRegister: true })
                            //         console.log(error);
                            //     });
                        }}
                        validationSchema={Yup.object().shape({
                            card_number: Yup.string()
                                .required('Campo Número do Cartão deve ser preenchido'),
                            name: Yup.string()
                                .required('Campo Nome deve ser preenchido'),
                            cvc: Yup.string()
                                .required('Campo CVC deve ser preenchido'),
                            identity_document: Yup.string()
                                .required('Campo CPF deve ser preenchido'),
                            ddd: Yup.string()
                                .required('Campo DDD deve ser preenchido'),
                            cell_phone: Yup.string()
                                .required('Campo Telefone deve ser preenchido'),
                            birth_date: Yup.string()
                                .required('Campo Data de Nascimento deve ser preenchido'),
                            terms: Yup.bool()
                                .required('Campo Termo deve ser selecionado')
                                .test(
                                    'terms',
                                    'Campo Termo deve ser selecionado',
                                    value => value === true
                                )
                                .required('Campo Termo deve ser selecionado'),
                        })}>
                        {props => {
                            const {
                                values,
                                touched,
                                errors,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                            } = props;
                            return (
                                <form onSubmit={handleSubmit}>
                                    <div className="row box-default no-margin mt-3">
                                        <div className="col" style={{ paddingLeft: '0' }}>

                                            <label htmlFor="card_number" style={{ display: 'block' }}>
                                                Número do Cartão *
                                            </label>
                                            <MaskedInput
                                                id="card_number"
                                                placeholder="Digite o Número do Cartão"
                                                mask={cardNumber}
                                                type="text"
                                                value={values.card_number}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                className={
                                                    errors.card_number && touched.card_number ? 'text-input error' : 'text-input'
                                                }
                                            />
                                            {errors.card_number && touched.card_number && (
                                                <div className="input-feedback">{errors.card_number}</div>
                                            )}

                                            <label htmlFor="name" style={{ display: 'block' }}>
                                                Nome Impresso no Cartão *
                                            </label>
                                            <input
                                                id="name"
                                                placeholder="Digite o Nome Impresso no Cartão"
                                                type="text"
                                                value={values.name}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                className={
                                                    errors.name && touched.name ? 'text-input error' : 'text-input'
                                                }
                                            />
                                            {errors.name && touched.name && (
                                                <div className="input-feedback">{errors.name}</div>
                                            )}

                                            <label htmlFor="cvc" style={{ display: 'block' }}>
                                                CVC *
                                            </label>
                                            <input
                                                id="cvc"
                                                placeholder="Código de Segurança"
                                                type="text"
                                                value={values.cvc}
                                                onInput={(e) => this.setState({ cvc: e.target.value })}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                className={
                                                    errors.cvc && touched.cvc ? 'text-input error' : 'text-input'
                                                }
                                            />
                                            {errors.cvc && touched.cvc && (
                                                <div className="input-feedback">{errors.cvc}</div>
                                            )}

                                            <label htmlFor="identity_document" style={{ display: 'block' }}>
                                                CPF *
                                            </label>
                                            <MaskedInput
                                                id="identity_document"
                                                mask={cpfNumberMask}
                                                placeholder="Digite o CPF do Titular do Cartão"
                                                type="text"
                                                value={values.identity_document}
                                                onInput={(e) => this.setState({ cpf: e.target.value })}
                                                onChange={handleChange}
                                                onBlur={this.ValidaCPF.bind(this)}
                                                className={
                                                    errors.identity_document && touched.identity_document ? 'text-input error' : 'text-input'
                                                }
                                            />
                                            <i className={`fas fa-check ${this.state.isCPF === true ? "" : "hide"}`}></i>
                                            <i className={`fas fa-times ${this.state.isCPF === false ? "" : "hide"}`}></i>
                                            {errors.identity_document && touched.identity_document && (
                                                <div className="input-feedback">{errors.identity_document}</div>
                                            )}
                                            <div className={`input-feedback ${this.state.isCPF === false ? "" : "hide"}`}>Verifique o CPF</div>

                                            <label htmlFor="ddd" className="col" style={{ display: 'block', paddingLeft: '0' }}>
                                                DDD * <span style={{ marginLeft: '15%' }}>Telefone *</span>
                                            </label>
                                            <div className="row no-margin">
                                                <MaskedInput
                                                    id="ddd"
                                                    mask={dddNumberMask}
                                                    placeholder="DDD"
                                                    type="text"
                                                    value={values.ddd}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className={
                                                        errors.ddd && touched.ddd ? 'text-input error col-3' : 'text-input col-3'
                                                    }
                                                />

                                                <MaskedInput
                                                    style={{ marginLeft: '3%', maxWidth: '72%' }}
                                                    mask={phoneNumberMask}
                                                    id="cell_phone"
                                                    placeholder="Telefone"
                                                    type="text"
                                                    value={values.cell_phone}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    className={
                                                        errors.cell_phone && touched.cell_phone ? 'text-input error col-9' : 'text-input col-9'
                                                    }
                                                />
                                                {errors.ddd && touched.ddd && (
                                                    <div className="input-feedback">{errors.ddd}</div>
                                                )}
                                                {errors.cell_phone && touched.cell_phone && (
                                                    <div className="input-feedback">{errors.cell_phone}</div>
                                                )}
                                            </div>

                                            <label htmlFor="birth_date" style={{ display: 'block' }}>
                                                Data de Nascimento *
                                            </label>
                                            <MaskedInput
                                                id="birth_date"
                                                placeholder="Data de Nascimento"
                                                type="text"
                                                value={values.birth_date}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                mask={dataMask}
                                                className={
                                                    errors.birth_date && touched.birth_date ? 'text-input error' : 'text-input'
                                                }
                                                />
                                                {errors.birth_date && touched.birth_date && (
                                                    <div className="input-feedback">{errors.birth_date}</div>
                                                )}
                                        </div>
                                    </div>

                                    <h4 className="mt-3 text-center">Informe abaixo o endreço de cobrança do titular do cartão</h4>
                                    <hr />

                                    <div className="row box-default no-margin mt-3">
                                        <div className="col-8" style={{ paddingLeft: '0' }}>
                                            <label htmlFor="zipcode" style={{ display: 'block' }}>
                                                CEP *
                                            </label>
                                            <MaskedInput
                                                id="zipcode"
                                                placeholder="CEP"
                                                type="text"
                                                value={values.zipcode}
                                                onChange={handleChange}
                                                onBlur={this.CheckZipcode}
                                                onInput={(e) => this.setState({ zipcode: e.target.value })}
                                                mask={zipcodeMask}
                                                className={
                                                    errors.zipcode && touched.zipcode ? 'text-input error' : 'text-input'
                                                }
                                            />
                                            {this.state.validate_zipcode ? <div className="input-feedback">CEP deve ser preenchido</div> : null}
                                            {this.state.noZipcode ? <div className="input-feedback">CEP não encontrado</div> : null}
                                        </div>
                                        <div className="col-4 align-items" style={{ alignItems: 'flex-end', justifyContent: 'flex-end', paddingRight: '0' }}>
                                            <button className="btn btn-primary" type="button" style={{ margin: '0', marginBottom: '2px' }}><i className="fas fa-search" style={{ fontSize: '10px', marginRight: '5px' }}></i>Buscar</button>
                                        </div>

                                        <label htmlFor="address" style={{ display: 'block' }}>
                                            Rua ou Logradouro *
                                        </label>
                                        <input
                                            id="address"
                                            placeholder="Digite o endereço"
                                            type="text"
                                            value={this.state.street ? this.state.street : values.address}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={
                                                errors.address && touched.address ? 'text-input error' : 'text-input'
                                            }
                                        />
                                        {this.state.validate_street ? <div className="input-feedback">Endereço deve ser preenchido</div> : null}

                                        <label htmlFor="number" style={{ display: 'block' }}>
                                            Número *
                                        </label>
                                        <input
                                            id="number"
                                            placeholder="Número"
                                            type="text"
                                            value={values.number}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={
                                                errors.number && touched.number ? 'text-input error' : 'text-input'
                                            }
                                        />
                                        {this.state.validate_number ? <div className="input-feedback">Número deve ser preenchido</div> : null}

                                        <label htmlFor="complement" style={{ display: 'block' }}>
                                            Complemento
                                        </label>
                                        <input
                                            id="complement"
                                            placeholder="Complemento"
                                            type="text"
                                            value={this.state.complement ? this.state.complement : values.complement}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={
                                                errors.complement && touched.complement ? 'text-input error' : 'text-input'
                                            }
                                        />

                                        <label htmlFor="neighborhood" style={{ display: 'block' }}>
                                            Bairro *
                                        </label>
                                        <input
                                            id="neighborhood"
                                            placeholder="Bairro"
                                            type="text"
                                            value={this.state.neighborhood ? this.state.neighborhood : values.neighborhood}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={
                                                errors.neighborhood && touched.neighborhood ? 'text-input error' : 'text-input'
                                            }
                                        />
                                        {this.state.validate_neighborhood ? <div className="input-feedback">Bairro deve ser preenchido</div> : null}

                                        <label htmlFor="state" style={{ display: 'block' }}>
                                            Estado *
                                        </label>
                                        <select
                                            name="state"
                                            value={values.state}
                                            onChange={this.GetCities}
                                            onBlur={(e) => this.setState({ uf: e.target.value })}
                                            style={{ display: 'block' }}
                                            className={
                                                errors.state && touched.state ? 'form-control error' : 'form-control'
                                            }
                                        >
                                            <option value="" label="Selecione o Estado" />
                                            {this.state.states.map((estado, i) => {
                                                if (this.state.uf === estado.uf) {
                                                    return <option selected value={estado.uf} label={estado.estado} key={i} />
                                                } else {
                                                    return <option value={estado.uf} label={estado.estado} key={i} />
                                                }
                                            }
                                            )}
                                        </select>
                                        {this.state.validate_uf ? <div className="input-feedback">Estado deve ser selecionado</div> : null}

                                        <label htmlFor="city" style={{ display: 'block' }}>
                                            Cidade *
                                        </label>
                                        <select
                                            name="city"
                                            value={values.city}
                                            onChange={(e) => this.setState({ city: e.target.value })}
                                            onBlur={handleBlur}
                                            style={{ display: 'block' }}
                                            className={
                                                errors.city && touched.city ? 'form-control error' : 'form-control'
                                            }
                                        >
                                            <option value="" label="Selecione a Cidade" />
                                            {this.state.cities.map((city, i) => {
                                                if (this.state.id_city === city.id_cidade) {
                                                    return <option selected value={city.id_cidade} label={city.cidade} key={i} />
                                                } else {
                                                    return <option value={city.id_cidade} label={city.cidade} key={i} />
                                                }
                                            }
                                            )}
                                        </select>
                                        {this.state.validate_city ? <div className="input-feedback">Cidade deve ser selecionada</div> : null}
                                    </div>

                                    <div className="row box-default no-margin">
                                        <div className="col no-padding">
                                            <label htmlFor="city" style={{ display: 'block' }}>
                                                Pagar em
                                            </label>
                                            <select
                                                name="city"
                                                value={values.installments}
                                                // onChange={(e) => this.setState({ installments: e.target.value })}
                                                onChange={this.onChangeInstallments}
                                                onBlur={handleBlur}
                                                style={{ display: 'block' }}
                                                className={
                                                    errors.installments && touched.installments ? 'form-control error' : 'form-control'
                                                }
                                            >
                                                {
                                                    this.state.ar_installments.map((installment, i) =>
                                                    {
                                                        if(i === 0){
                                                            return <option key={i} value={`${installment}${separator}${i}`} label={`${parseInt(i + 1)}x sem juros`}  />
                                                        } else{
                                                            return <option key={i} value={`${installment}${separator}${i}`} label={`R$ ${parseInt(i + 1)}x ${installment } c/ juros`}  />
                                                        }
                                                    }
                                                    )
                                                }                                                
                                            </select>
                                            <div>
                                                {
                                                    this.state.showWarning ? 
                                                        <h6 className="installments-warning">
                                                            AS COBRANÇAS DOS ENCARGOS DE VENDAS PARCELADAS SÃO DETERMINADAS PELA INSTITUIÇÃO EMISSORA DO SEU CARTÃO DE CRÉDITO, SENDO ELA A ÚNICA RESPONSÁVEL E BENEFICIÁRIA DOS MESMOS.
                                                        </h6> 
                                                    : null
                                                }
                                                <h3 className="total-interest">
                                                   Total R$ { this.state.totalInterest }
                                                </h3>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div className="row box-default no-margin">
                                        <div className="col no-padding">
                                            <div className="custom-control custom-checkbox">
                                                <input type="checkbox" className="custom-control-input" id="save_card" name="save_card" onChange={handleChange} />
                                                <label className="custom-control-label" htmlFor="save_card">
                                                    Salvar cartão para futuras compras
                                                </label>
                                            </div>

                                            <div className="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" className="custom-control-input" id="terms" name="terms" onChange={handleChange} />
                                                <label className="custom-control-label" htmlFor="terms">Li e concordo com os termos de uso <strong>*</strong></label>
                                                {errors.terms && touched.terms && (
                                                    <div className="input-feedback">{errors.terms}</div>
                                                )}
                                            </div>

                                            <Link to="/" className="btn button-block mb-3" style={{ background: 'orange', padding: '12px 20px', color: '#fff', fontWeight: 'bold' }}  data-toggle="modal" data-target="#modalTerms">
                                                Termos de uso
                                            </Link>

                                            <button className={`button-block`} type="submit" style={{ margin: '0' }}>
                                                Efetuar Pagamento
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            );
                        }}
                    </Formik>
                </div>
                {/* TERMS */}
                <div className="modal fade" id="modalTerms" tabIndex="-1" role="dialog" aria-labelledby="modalTermsLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Termos de Uso</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body" dangerouslySetInnerHTML={{__html:this.state.terms}}></div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* TERMS */}
            </div>
        )
    }
}

export default Checkout;