import React, { Component } from 'react';

// const UserName = ({ showName, userName }) => {
//     return(
//         // <h4 className="ticket-user-name">{showName == true ? userName : null}</h4>
//         <h4 className="ticket-user-name">
//             {this.props.name}
//         </h4>
//     ) 
// }

// export default UserName;

export default class UserName extends Component{
    render() {
        return(
            <h4 className="ticket-user-name">
                {this.props.name}
            </h4>
        );
    }
}