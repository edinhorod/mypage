import React, { Component } from 'react';
import api from '../../components/Util/api';
import { isAuthenticated } from '../../components/Util/Auth';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import './Checkout.css';

const separator = '/';

export default class Ticket extends Component {
    constructor(props){
        super(props);
        this.state = {
            toggleButton: true,
            userName: '',
            toGift: false,
        }
    }

    componentDidMount(){
        if (isAuthenticated()) {
            const user_info = JSON.parse(localStorage.getItem('user_info'));
            this.setState({ 
                user_name: user_info.name,
                user_identity_document: user_info.identity_document,
                auth_token: user_info.auth_token,
            });
        }
    }

    ticketsCheckDocument = (e) => {
        const [ticketUniqueNumber, lotType] = e.target.value.split(separator);
        console.log(ticketUniqueNumber);
        api.post('https://web-services.yeapps.com.br/api/tickets/check/document', {
            identity_document: this.state.user_identity_document,
            unique_number_ticket: ticketUniqueNumber
        }).then(res => {
            if (lotType == res.data.data.gender || lotType == 'U') {
                console.log('É mesmo genero');
                this.setState({ 
                    showName: true,
                    userName: res.data.data.name,
                    toggleButton: false 
                })
            } else {
                console.log('NÃO É o mesmo genero');
            }
        })
    }

    ticketChange = () => {
        this.setState({ 
            toggleButton: true,
            userName: "",
        });
    }

    toGift = () => {
        this.setState({
            toGift: true,
        })
    }

    render() {
        const { lot, index, userGender } = this.props;
        return (
            <div className="box-ticket">
                <SweetAlert 
                    show={this.state.toGift} 
                    title="Enviar Convite"
                    text="Digite o CPF"
                    type="input"
                    inputType="tel"
                    inputPlaceholder="Digite o CPF"
                    onConfirm={
                        inputValue => {
                        api.post('https://web-services.yeapps.com.br/api/tickets/check/document', {
                            identity_document: inputValue,
                            unique_number_ticket: lot.ticketUniqueNumber
                        }).then(res => {
                            console.log(res.data.success)
                            console.log(res.data.data)
                            this.setState({ toGift: false });
                        }).catch(error => {
                            console.log(error)
                        })
                      }}
                    showCancelButton={true}
                    cancelButtonText="Cancelar"
                    onCancel={() => this.setState({ toGift: false })}
                />
          
                <div className="box-vertical">
                    <h4 className="ticket-type">
                        { lot.lotType === 'U' ? 'Unissex' : lot.lotType === 'M' ? 'Masculino' : 'Feminino' }
                        <br />
                        <small>R$ { parseInt(lot.lotPrice).toFixed(2).replace('.', ',') }</small>
                    </h4>
                </div>
                <div className="box-text">
                    <h4 className="ticket-user-name">{this.state.userName}</h4>
                    <p className="text-center">
                        Os convites são nominais. <br />
                        Indique abaixo quem vai utilizar este convite
                    </p>
                    <div className="box-button">
                        {
                            this.state.toggleButton ? 
                            (
                                <div>
                                    {
                                        lot.lotType == userGender || lot.lotType == 'U' ?
                                            <button  type="button" className="btn btn-primary"  value={`${lot.ticketUniqueNumber}${separator}${lot.lotType}`} onClick={this.ticketsCheckDocument}>
                                                É Meu
                                            </button>
                                        :
                                            null
                                    }
                                    <button type="button" className="btn btn-primary" onClick={this.toGift}>
                                        Vou Presentear
                                    </button>
                                </div>
                            ) : 
                            (
                                <button type="button" className="btn btn-primary" onClick={this.ticketChange}>
                                    Alterar
                                </button>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
