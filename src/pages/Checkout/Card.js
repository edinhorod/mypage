import React, { Component } from 'react';
import './Checkout.css';
import './UserName';
import '../../components/Css/App.css';
import { isAuthenticated } from '../../components/Util/Auth';
import Ticket from './Ticket';

class Card extends Component{
    constructor(props){
        super(props);
        this.state = {
            lot: [],
            lots: [],
            userName: '',
            user_identity_document: '',
            user: '',
            toggleButton: true,
            userGender: '',
        }
    }
    
    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const {
            events,
            events: { tickets },
            total
        } = cart;
        this.setState({ 
            cart,
            events,
            event_name: events.event_name,
            priceTotal: total.price,
            lots: tickets.lot
        });
        if (isAuthenticated()) {
            const user_info = JSON.parse(localStorage.getItem('user_info'));
            this.setState({ 
                user_name: user_info.name,
                user_identity_document: user_info.identity_document,
                auth_token: user_info.auth_token,
                userGender: user_info.gender
            })
        }
        
        // ATUALIZANDO OS TICKETS
        var ar_lot = [];
        tickets.lot.forEach(function (item) {
            for (var i = 0; i < item.quantity; i++) {
                var item_lot = {
                    ticketName: item.ticketName,
                    ticketUniqueNumber: item.ticketUniqueNumber,
                    ticketPrevenda: item.ticketPrevenda,
                    lotPrice: item.lotPrice,
                    lotPriceTax: item.lotPrice,
                    lotType: item.lotType,
                    lotUniqueNumber: item.lotUniqueNumber,
                    lotNumber: item.lotNumber,
                    quantity: item.quantity,
                    totalLotPrice: item.totalLotPrice,
                    total: item.total,
                    user_name: 'Edinho',
                    identity_document: null,
                    email: null
                }
                ar_lot.push(item_lot)
            }
        })
        this.setState({ lot: ar_lot })
        // ATUALIZANDO OS TICKETS
    }

    render() {
        const lots = this.state.lot.map((lot, l) => (<Ticket index={l} lot={lot} key={l} userGender={this.state.userGender} />));
        return(
            <div>
                {lots}
            </div>
        )
    }
}

export default Card;