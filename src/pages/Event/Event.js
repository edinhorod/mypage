import React, { Component } from 'react';
import api from '../../components/Util/api.js';//para requisições
import { Link } from 'react-router-dom';
import Header from '../../components/Header/Header';
import Lot from './Lot';
import Tabbar from '../../components/Tabbar/Tabbar';

import './Event.css';
import '../../components/Css/App.css';

import festa01 from '../../images/festa01.jpg';

const separator = '/';

const event = {
    "id": 84,
    "name": "Evento de Teste 3",
    "installments": "",
    "instructions": null,
    "place": "Casa",
    "bullet_app": festa01,
    "banner_app": festa01,
    "cover_app": festa01,
    "video": null,
    "banner": festa01,
    "unique_number": "3WZ7xZ4a2aaWcy737dwZ3b33b5y435",
    "max_purchase": "4",
    "dates": [
        {
            "date": "20\/05\/2019",
            "unique_number": "3WZ7xZ4a2aaWcy737dwZ3b33b5y435",
            "tickets": [
                {
                    "name": "VIP",
                    "unique_number": "DYYc8DYWbaC21DzZAa1ZbccbaBy7bd",
                    "half_ticket": null,
                    "prevenda": false,
                    "info": null,
                    "imagem": festa01,
                    "lot": [
                        {
                            "unique_number": "75W3A8729x297bWC6d0c7CyxC21ZC7",
                            "type": "U",
                            "name": "Unissex",
                            "lot_number": 1,
                            "price": "10.00",
                            "quantity": "100",
                            "sold": 99,
                            "unique_number_lot": "75W3A8729x297bWC6d0c7CyxC21ZC7_U"
                        }
                    ]
                }
            ]
        }
    ],
    "date_expiration": "2019-05-15",
    "description": "<p>Em breve, aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa. Aqui vai uma descrição da festa.<\/p>\r\n"
}

class Event extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event: {},
            tickets: [],
            ticket: {},
            selectedEvent: {},
            dates: [],
            selectedQuantities: {},
            quantities: {},
            maxTotalItems: 0,
            qtd: 0,
            totalZero: false,
            priceTotal: 0,
        }
    }

    async componentDidMount() {
        //The Event come from a API, here I put a static object
        await this.setState({
            dates: event.dates,
            event: event
        })

        this.state.dates.map((date, i) =>
            this.setState({ tickets: this.state.dates[i].tickets })
        )

        this.state.tickets.map((ticket, i) =>
            this.setState({ lots: ticket.lot })
        )

        this.setState({ maxTotalItems: this.state.event.max_purchase });//The event have a max purchase, so when reaches the limit, the increment button must disappear

    }

    onQuantityChange = (ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, newValue) => {
        this.setState(prevState => {
            this.setState({
                selectedQuantities: { ...prevState.selectedQuantities, [`${ticketName}${separator}${ticketPrevenda}${separator}${ticketUniqueNumber}${separator}${lotType}${separator}${lotNumber}${separator}${lotUniqueNumber}${separator}${lotPrice}${separator}${lotPriceTax}`]: newValue },
            })            
        }, () => {
            const selectedArray = Object.entries(this.state.selectedQuantities).map(
                ([key, quantity]) => {
                    const [ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax] = key.split(separator)
                    const totalLotPrice = parseFloat(lotPrice + lotPriceTax);
                    const total = parseFloat(totalLotPrice * quantity);
                    return {
                        ticketName,
                        ticketPrevenda,
                        ticketUniqueNumber,
                        lotType,
                        lotNumber,
                        lotUniqueNumber,
                        lotPrice,
                        lotPriceTax,
                        quantity,
                        totalLotPrice,
                        total
                    }
                },
            )
            console.log(this.state.selectedQuantities);

            //SOMANDO A QTD E ATUALIZANDO O STATE
            var lotQuantity = selectedArray.reduce(function (prevVal, elem) {
                const lotQuantity = prevVal + elem.quantity;
                return lotQuantity;
            }, 0);
            this.setState({ qtd: lotQuantity });

            //SOMANDO O TOTAL E ATUIALIZANDO O STATE
            var total = selectedArray.reduce(function (prevVal, elem) {
                const total = prevVal + elem.total;
                return total;
            }, 0);
            this.setState({ priceTotal: total })

            //MOSTRAR/OCULTAR FOOTER
            if (lotQuantity > 0) {
                this.setState({ totalZero: true });
            } else {
                this.setState({ totalZero: false });
            }

            //OBJETO CART
            var lot = selectedArray;
            var tickets = {
                name: ticketName,
                prevenda: ticketPrevenda,
                unique_number: ticketUniqueNumber,
                lot: lot
            }

            total = {
                price: total,
                totalQuantity: lotQuantity,
            };

            var events = {
                banner_app: this.state.event.banner_app,
                installments: this.state.event.installments,
                max_purchase: this.state.event.max_purchase,
                name: this.state.event.name,
                tickets: tickets,
                unique_number: this.state.event.unique_number,
                total_tickets: lotQuantity
            };
            var cart = { events: events, total: total };
            localStorage.setItem('cart', JSON.stringify(cart));
            localStorage.setItem('qtd', JSON.stringify(lotQuantity));
            // console.log(lotQuantity);            
            // console.log(JSON.parse(localStorage.getItem('cart')));
            //OBJETO CART
        })
    }

    getSelectedQuantity = (ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax) => this.state.selectedQuantities[`${ticketName}${separator}${ticketPrevenda}${separator}${ticketUniqueNumber}${separator}${lotType}${separator}${lotNumber}${separator}${lotUniqueNumber}${separator}${lotPrice}${separator}${lotPriceTax}`];

    getAdditionEnabled = () => Object.values(this.state.selectedQuantities).reduce((acc, i) => acc + i, 0) < this.state.maxTotalItems;//The event have a max purchase, so when reaches the limit, the increment button must disappear

    onCheckoutButtonClick = () => {
        const selectedArray = Object.entries(this.state.selectedQuantities).map(
            ([key, quantity]) => {
                const [ticketUniqueNumber, lotUniqueNumber] = key.split(separator)
                return {
                    ticketUniqueNumber,
                    lotUniqueNumber,
                    quantity,
                }
            },
        )
        console.log(selectedArray);
    }

    render() {
        return (
            <div>
                <Tabbar />
                <Header Title={this.state.event.name} ToPage="/" />
                <div className="container-fluid padding-15 event">
                    <div className="mt-5">
                        <img className="card-img-top" src={this.state.event.banner_app} alt={this.state.event.name} />
                        <div className="row no-margin mb-3">
                            <div className="col-8 no-padding">
                                <h1 className="event-title">{this.state.event.name}</h1>
                                <h1 className="event-place">
                                    <i className="fas fa-pin"></i>
                                    {this.state.event.place}
                                </h1>
                            </div>
                            <div className="col-4 event-date-col align-items">
                                <span className="event-date" id="event-date">
                                </span>
                                {this.state.dates.map((date, i) =>
                                    <span className="event-date" key={i}>
                                        {date.date}
                                    </span>
                                )}
                            </div>
                        </div>

                        {
                            this.state.tickets.map((ticket, i) =>
                                (
                                    <div key={i}>
                                        <div className="row">
                                            <div className="col">
                                                <h3 className="ticket-name">{ticket.name}</h3>
                                            </div>
                                        </div>
                                        {ticket.lot.map((lot, l) =>
                                            <Lot
                                                key={l}
                                                events={this.state.event}
                                                ticketName={ticket.name}
                                                ticketPrevenda={ticket.prevenda}
                                                ticketUniqueNumber={ticket.unique_number}
                                                lotUniqueNumber={lot.unique_number}
                                                lotName={lot.name}
                                                lotType={lot.type}
                                                lotNumber={lot.lot_number}
                                                lotPrice={lot.price}
                                                lotPriceTax={lot.price_tax}
                                                onQuantityChange={this.onQuantityChange}
                                                maxPurchase={this.state.event.max_purchase}
                                                quantity={this.getSelectedQuantity(ticket.name, ticket.prevenda, ticket.unique_number, lot.type, lot.lot_number, lot.unique_number, lot.price, lot.price_tax)}
                                                additionEnabled={this.getAdditionEnabled()}
                                            />
                                        )}
                                        <hr />
                                    </div>
                                )
                            )
                        }

                        <div className="row mt-5">
                            <div className="col">
                                <h6 className="text-default">Descrição</h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col" dangerouslySetInnerHTML={{ __html: this.state.event.description }}></div>
                        </div>
                    </div>
                </div>
                <div className={
                    this.state.totalZero > 0 ? 'row cart-footer' : 'row cart-footer height-0'
                }>
                    <div className="col col-price">
                        <h6>{this.state.qtd} INGRESSO{this.state.qtd === 1 ? '' : 'S'}</h6>
                        <h5>R$ {this.state.priceTotal.toFixed(2).replace('.', ',')}</h5>
                    </div>
                    <Link className="col col-purchase" to="/cart">
                        Comprar
                    </Link>
                </div>
            </div>
        )
    }
}

export default Event;