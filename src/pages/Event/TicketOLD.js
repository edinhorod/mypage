import React from 'react'

const Ticket = ({ children, name }) => (
    <div>
        <p>{name}</p>
        <div>{children}</div>
    </div>
)

export default Ticket