import React, { Component } from 'react';
import api from '../../components/Util/api.js';//para requisições
import Header from '../../components/Header/Header';
import Lot from './Lot'

import './Event.css';
import '../../components/Css/App.css';

const separator = '/';
class Event extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event: {},
            selectedEvent: {},
            dates: [],
            tickets: [],
            selectedQuantities: {},
            quantities: {},
            maxTotalItems: 0,
            qtd: 0,
            total: {
                price: 0,
                quantity: 0,
            },
            totalZero: false,
            lotPrice: [],
        }
    }

    async componentDidMount() {
        const { id } = this.props.match.params;
        await api.get(`event/${id}`)
            .then(res => {
                const event = res.data.data;
                this.setState({ event });
                this.setState({ dates: event.dates })

                this.state.dates.map((date, i) =>
                    this.setState({ tickets: this.state.dates[i].tickets })
                )

                this.state.tickets.map((ticket, i) =>
                    this.setState({ lots: ticket.lot })
                )
                this.setState({
                    selectedQuantities: {},
                    quantities: {},
                })
                this.setState({ maxTotalItems: this.state.event.max_purchase });
                // console.log(this.state.event);
            })
    }

    onQuantityChange = (ticketUniqueNumber, lotUniqueNumber, lotPrice, lotPriceTax, newValue) => {
        this.setState(prevState => {
            this.setState({
                // selectedQuantities: { ...prevState.selectedQuantities, [this.getSelectedStateKey(ticketUniqueNumber, lotUniqueNumber)]: newValue, },
                selectedQuantities: { ...prevState.selectedQuantities, [`${ticketUniqueNumber}${separator}${lotUniqueNumber}`]: newValue, },
            })
        }, () => {
            // console.log(this.state.selectedQuantities);
            const selectedArray = Object.entries(this.state.selectedQuantities).map(
                ([key, quantity]) => {
                    const [ticketUniqueNumber, lotUniqueNumber] = key.split(separator)
                    return {
                        ticketUniqueNumber,
                        lotUniqueNumber,
                        quantity,
                    }
                },
            )

            console.log(lotPrice);
            
            
            var sum = selectedArray.reduce(function(prevVal, elem) {
                return prevVal + elem.quantity;
            }, 0);
            this.setState({ qtd: sum });

            if (sum > 0) {
                this.setState({ totalZero: true });
            } else {
                this.setState({ totalZero: false });
            }
            // var events = {
            //     banner_app: this.state.event.banner_app,
            //     installments: this.state.event.installments,
            //     max_purchase: this.state.event.max_purchase,
            //     name: this.state.event.name,
            //     unique_number: this.state.event.unique_number
            // };
            // var cart = { events: events, total: total };
            // console.log(events);
        })
        // console.log(this.state.selectedQuantities);
    }

    // getSelectedStateKey = (ticketUniqueNumber, lotUniqueNumber) =>
    //     `${ticketUniqueNumber}${separator}${lotUniqueNumber}`

    // getSelectedQuantity = (ticketUniqueNumber, lotUniqueNumber) =>
    //     this.state.selectedQuantities[
    //         this.getSelectedStateKey(ticketUniqueNumber, lotUniqueNumber)
    //     ]

    getSelectedQuantity = (ticketUniqueNumber, lotUniqueNumber) => this.state.selectedQuantities[`${ticketUniqueNumber}${separator}${lotUniqueNumber}`];

    getAdditionEnabled = () => Object.values(this.state.selectedQuantities).reduce((acc, i) => acc + i, 0) < this.state.maxTotalItems;

    onCheckoutButtonClick = () => {
        const selectedArray = Object.entries(this.state.selectedQuantities).map(
            ([key, quantity]) => {
                const [ticketUniqueNumber, lotUniqueNumber] = key.split(separator)
                return {
                    ticketUniqueNumber,
                    lotUniqueNumber,
                    quantity,
                }
            },
        )
        console.log(selectedArray);
    }

    render() {
        return (
            <div>
                <Header Title={this.state.event.name} ToPage="/" />
                <div className="container-fluid padding-15 event">
                    <div className="mt-5">
                        <img className="card-img-top" src={this.state.event.banner_app} alt={this.state.event.name} />
                        <div className="row no-margin mb-3">
                            <div className="col-8 no-padding">
                                <h1 className="event-title">{this.state.event.name}</h1>
                                <h1 className="event-place">
                                    <i className="fas fa-pin"></i>
                                    {this.state.event.place}
                                </h1>
                            </div>
                            <div className="col-4 event-date-col align-items">
                                <span className="event-date" id="event-date">
                                </span>
                                {this.state.dates.map((date, i) =>
                                    <span className="event-date" key={i}>
                                        {date.date}
                                    </span>
                                )}
                            </div>
                        </div>

                        {
                            this.state.tickets.map((ticket, i) =>
                                (
                                    <div key={i}>
                                        <div className="row">
                                            <div className="col">
                                                <h3 className="ticket-name">{ticket.name}</h3>
                                            </div>
                                        </div>
                                        {ticket.lot.map((lot, l) =>
                                            <Lot
                                                key={l}
                                                events={this.state.event}
                                                lotUniqueNumber={lot.unique_number}
                                                ticketUniqueNumber={ticket.unique_number}
                                                lotName={lot.name}
                                                lotPrice={lot.price}
                                                lotPriceTax={lot.price_tax}
                                                onQuantityChange={this.onQuantityChange}
                                                maxPurchase={this.state.event.max_purchase}
                                                quantity={this.getSelectedQuantity(ticket.unique_number, lot.unique_number)}
                                                additionEnabled={this.getAdditionEnabled()}
                                            />
                                        )}
                                        <hr />
                                    </div>
                                )
                            )
                        }

                        <div className="row mt-5">
                            <div className="col">
                                <h6 className="text-default">Descrição</h6>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col" dangerouslySetInnerHTML={{ __html: this.state.event.description }}></div>
                        </div>
                    </div>
                </div>
                <div className={
                    this.state.totalZero > 0 ? 'row cart-footer' : 'row cart-footer height-0'
                }>
                    <div className="col col-price">
                        <h6>{this.state.qtd} INGRESSOS</h6>
                        <h5>R$ 16,00</h5>
                    </div>
                    <button className="col col-purchase" onClick={this.onCheckoutButtonClick}>
                        Comprar
                    </button>
                </div>
            </div>
        )
    }
}

export default Event;