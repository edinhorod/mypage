import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import api from '../../components/Util/api.js';//para requisições
import Tabbar from '../../components/Tabbar/Tabbar';
import Header from '../../components/Header/Header';

import './Home.css';
import '../../components/Css/App.css';

import festa01 from '../../images/festa01.jpg';
import festa02 from '../../images/festa02.jpg';
import festa03 from '../../images/festa03.jpg';
import festa04 from '../../images/festa04.jpg';
import festa05 from '../../images/festa05.jpg';

const events = [
    {
        "id": 84,
        "name": "Evento de Teste 3",
        "date": "20\/05\/2019",
        "unique_number": "3WZ7xZ4a2aaWcy737dwZ3b33b5y435",
        "place": "Casa",
        "date_expiration": "2019-05-15",
        "bullet_app": festa01,
        "banner_app": festa01,
        "cover_app": festa01
    },
    // {
    //     "id": 127,
    //     "name": "Yeapps é só alegria",
    //     "date": "27\/07\/2019",
    //     "unique_number": "7AzwCw2BZ88a9B01Z28c03Z3684DBc",
    //     "place": "Aqui",
    //     "date_expiration": "2019-07-27",
    //     "bullet_app": festa04,
    //     "banner_app": festa04,
    //     "cover_app": festa04
    // }
];

const tickets = [
    {
        "id": 84,
        "name": "Evento de Teste 3",
        "unique_number": "3WZ7xZ4a2aaWcy737dwZ3b33b5y435",
        "image": festa01,
        "place": "Casa",
        "tickets": [
            {
                "ticket_name": "VIP",
                "cart_unique_number": "aBwD5945Z52x6a8466xx6Bb6Z5D8z9",
                "voucher": {
                    "voucher": "C76T30JY7S4DUIFN",
                    "event_name": "Evento de Teste 3",
                    "image": festa01,
                    "ticket_name": "VIP",
                    "user_name": "EDINHO RODRIGUES",
                    "event_date": "20\/05\/2019",
                    "identity_document": "32127675886",
                    "gender": "Unissex"
                }
            }
        ]
    },
    // {
    //     "id": 127,
    //     "name": "Yeapps é só alegria",
    //     "unique_number": "7AzwCw2BZ88a9B01Z28c03Z3684DBc",
    //     "image": festa02,
    //     "place": "Aqui",
    //     "tickets": [
    //         {
    //             "ticket_name": "Total flex",
    //             "cart_unique_number": "Cd7660cDY34ZY4xwDAxzD839BwAZzx",
    //             "voucher": {
    //                 "voucher": "08B9FNK0CF013R1Y",
    //                 "event_name": "Yeapps é só alegria",
    //                 "image": festa02,
    //                 "ticket_name": "Total flex",
    //                 "user_name": "EDINHO RODRIGUES",
    //                 "event_date": "27\/07\/2019",
    //                 "identity_document": "32127675886",
    //                 "gender": "Unissex"
    //             }
    //         },
    //         {
    //             "ticket_name": "Total flex",
    //             "cart_unique_number": "81zAyB7Zwwxbw50x03Z3x7507YD9CZ",
    //             "voucher": {
    //                 "voucher": "0ZR2BDAA3D6FMC8K",
    //                 "event_name": "Yeapps é só alegria",
    //                 "image": festa02,
    //                 "ticket_name": "Total flex",
    //                 "user_name": "EDINHO RODRIGUES",
    //                 "event_date": "27\/07\/2019",
    //                 "identity_document": "32127675886",
    //                 "gender": "Unissex"
    //             }
    //         },
    //         {
    //             "ticket_name": "Total flex",
    //             "cart_unique_number": "wzA8Y454AxzAb06cZAcdW7B6xC8z2A",
    //             "voucher": {
    //                 "voucher": "R472X8P78E1Q3AK1",
    //                 "event_name": "Yeapps é só alegria",
    //                 "image": festa02,
    //                 "ticket_name": "Total flex",
    //                 "user_name": "EDINHO RODRIGUES",
    //                 "event_date": "27\/07\/2019",
    //                 "identity_document": "32127675886",
    //                 "gender": "Unissex"
    //             }
    //         },
    //         {
    //             "ticket_name": "Total flex",
    //             "cart_unique_number": "C24cB96d6DZ5363b082Cx793B9BB1A",
    //             "voucher": {
    //                 "voucher": "KDWWYFD6QRDI3NR7",
    //                 "event_name": "Yeapps é só alegria",
    //                 "image": festa02,
    //                 "ticket_name": "Total flex",
    //                 "user_name": "EDINHO RODRIGUES",
    //                 "event_date": "27\/07\/2019",
    //                 "identity_document": "32127675886",
    //                 "gender": "Unissex"
    //             }
    //         }
    //     ]
    // }
];

class Home extends Component {
    
    state = {
        events: []
    }
    componentDidMount() {
        this.setState({
            events
        })
    }
    render() {
        
		return (
            <div>
                <Tabbar />
                <Header Title="Home" />
                <div className="container-fluid home">
                    <div className="has-top-home">
                    { 
                        this.state.events.map((event, i) => 
                            <div className="card" key={i}>
                                <img className="card-img-top" src={ event.banner_app } alt="{event.name}" />
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-8 no-padding-left">
                                            <h4 className="card-title">{event.name}</h4>
                                            <p className="card-date">{event.date}</p>
                                            <p className="card-place">{event.place}</p>
                                        </div>
                                        <div className="col-4 align-self-center justify-content-end">
                                            <Link className="btn btn-primary" to={'/event/' + event.id }>Comprar</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    )}
                    </div>
                </div>
            </div>
		)
	}

}

export default Home;