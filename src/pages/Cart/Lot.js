// import React from 'react'
// import ChooseQuantity from '../../components/ChooseQuantity/ChooseQuantity.js';

// const Lot = ({
//     ticketName,
//     ticketPrevenda,
//     lotUniqueNumber,
//     ticketUniqueNumber,
//     name,
//     lotType,
//     lotNumber,
//     lotPrice,
//     lotPriceTax,
//     quantity,
//     onQuantityChange,
//     additionEnabled,
//     maxPurchase,
//     lotQuantity,
// }) => {
//     const onQuantityChangeInternal = (newValue) => {
//         onQuantityChange(ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, newValue, maxPurchase, lotQuantity)
//     }
//     return (
//         <div className="row">
//             <div className="col-8">
//                 <h5 className="lot-name">{name}</h5>
//                 <h5 className="lot-name">
//                     {
//                         lotType === 'U' ? 'Unissex ' : ''
//                     }
//                     {
//                         lotType === 'M' ? 'Masculino ' : ''
//                     }
//                     {
//                         lotType === 'F' ? 'Feminino ' : ''
//                     }
//                     ({lotNumber}º Lote)
//                 </h5>
//                 <h6 className="lot-price">
//                     R$ {lotPrice.replace('.', ',')} <br />
//                     <small>(R$ {lotPrice.replace('.', ',')} + R$ {lotPriceTax.replace('.', ',')})</small>
//                 </h6>
//             </div>
//             <div className="col-4">
//                 <ChooseQuantity
//                     value={quantity}
//                     onChange={onQuantityChangeInternal}
//                     additionEnabled={additionEnabled}
//                     maxPurchase={maxPurchase}
//                     lotPrice={lotPrice}
//                     lotPriceTax={lotPriceTax}
//                     lotQuantity={lotQuantity}
//                 />
//             </div>
//         </div>
//     )
// }

// export default Lot

import React from 'react'
import ChooseQuantity from '../../components/ChooseQuantity/ChooseQuantity.js';

const Lot = ({
    ticketName,
    ticketPrevenda,
    lotUniqueNumber,
    ticketUniqueNumber,
    name,
    lotType,
    lotNumber,
    lotPrice,
    lotPriceTax,
    lotQuantity,
    myLotQuantity,
    onQuantityChange,
    additionEnabled,
    maxPurchase,
}) => {
    const onQuantityChangeInternal = (newValue) => {
        onQuantityChange(ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, myLotQuantity, newValue)
    }
    return (
        <div className="row">
            <div className="col-8">
                <h5 className="lot-name">{name}</h5>
                <h5 className="lot-name">
                    {
                        lotType === 'U' ? 'Unissex ' : ''
                    }
                    {
                        lotType === 'M' ? 'Masculino ' : ''
                    }
                    {
                        lotType === 'F' ? 'Feminino ' : ''
                    }
                    ({lotNumber}º Lote)
                </h5>
                <h6 className="lot-price">
                    R$ {lotPrice.replace('.', ',')} <br />
                    <small>(R$ {lotPrice.replace('.', ',')} + R$ {lotPriceTax.replace('.', ',')})</small>
                </h6>
            </div>
            <div className="col-4">
                <ChooseQuantity
                    value={myLotQuantity}
                    onChange={onQuantityChangeInternal}
                    additionEnabled={additionEnabled}
                    maxPurchase={maxPurchase}
                    lotPrice={lotPrice}
                    lotPriceTax={lotPriceTax}
                    lotQuantity={lotQuantity}
                />
            </div>
        </div>
    )
}

export default Lot