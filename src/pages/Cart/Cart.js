import React, { Component } from 'react';
import Lot from '../Cart/Lot';
import { Link } from 'react-router-dom';
import Tabbar from '../../components/Tabbar/Tabbar';

import './Cart.css';
import '../../components/Css/App.css';

import Header from '../../components/Header/Header';
//INSTRUCIONS
// The Event part is working perfectly. In cart I need to fetch items saved in localStorage, render on screen with set quantities and totals and change (increment/decrement) cart items and update localStorage
//INSTRUCIONS

const separator = '/';
class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            lot: [],
            qtd: 0,
            events: null,
            banner_app: null,
            event_name: null,
            priceTotal: null,
            quantity: null,
            selectedQuantities: {},
            maxTotalItems: 0,
            installments: '',
        }
    }
    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        
        const {
            events,
            events: { tickets },
            total
          } = cart;
        this.setState({
            cart,
            events,
            banner_app: events.banner_app,
            event_name: events.event_name,
            priceTotal: total.price,
            quantity: total.totalQuantity,
            lot: tickets.lot,
            maxTotalItems: events.max_purchase,
            selectedLots: tickets.lot,
        });
        console.log(events)
    }

    onQuantityChange = (ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, myLotQuantity, newValue) => {        
        console.log(this.state.selectedQuantities);
        this.setState(prevState => {
            this.setState({
                selectedQuantities: { ...prevState.selectedQuantities, [`${ticketName}${separator}${ticketPrevenda}${separator}${ticketUniqueNumber}${separator}${lotType}${separator}${lotNumber}${separator}${lotUniqueNumber}${separator}${lotPrice}${separator}${lotPriceTax}${separator}${myLotQuantity}`]: newValue },
            })
        }, () => {
            console.log(this.state.selectedQuantities);
            const selectedArray = Object.entries(this.state.selectedQuantities).map(
                ([key, quantity]) => {
                    const [ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, myLotQuantity] = key.split(separator)
                    const totalLotPrice = parseFloat(lotPrice + lotPriceTax);
                    const total = parseFloat(totalLotPrice * quantity);
                    return {
                        ticketName,
                        ticketPrevenda,
                        ticketUniqueNumber,
                        lotType,
                        lotNumber,
                        lotUniqueNumber,
                        lotPrice,
                        lotPriceTax,
                        quantity,
                        myLotQuantity,
                        totalLotPrice,
                        total
                    }
                },
            )
            // console.log(selectedArray);
            // console.log(this.state.quantity);

            //SOMANDO A QTD E ATUALIZANDO O STATE
            var lotQuantity = selectedArray.reduce(function(prevVal, elem) {
                console.log(elem.quantity)
                const lotQuantity = prevVal + elem.quantity;
                return lotQuantity;
            }, 0);
            this.setState({ qtd: lotQuantity });
            console.log(this.state.lotQuantity);

            // //SOMANDO O TOTAL E ATUIALIZANDO O STATE
            var total = selectedArray.reduce(function(prevVal, elem) {
                const total = prevVal + elem.total;
                return total;
            }, 0);
            this.setState({priceTotal: total})            

            //MOSTRAR/OCULTAR FOOTER
            if (lotQuantity > 0) {
                this.setState({ totalZero: true });
            } else {
                this.setState({ totalZero: false });
            }

            //OBJETO CART
            var lot = selectedArray;
            var tickets = {
                name: ticketName,
                prevenda: ticketPrevenda,
                unique_number: ticketUniqueNumber,
                lot: lot
            }

            total = {
                price: total,
                quantity: lotQuantity,
            };

            var events = {
                banner_app: this.state.events.banner_app,
                installments: this.state.events.installments,
                max_purchase: this.state.events.max_purchase,
                name: this.state.events.name,
                tickets: tickets,
                unique_number: this.state.events.unique_number,
                total_tickets: lotQuantity
            };
            var cart = { events: events, total: total };
            localStorage.setItem('cart', JSON.stringify(cart));
            localStorage.setItem('qtd', JSON.stringify(lotQuantity));
            console.log(lotQuantity);            
            console.log(JSON.parse(localStorage.getItem('cart')));
            //OBJETO CART
        })
    }


    // getSelectedQuantity = (ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax) => this.state.selectedQuantities[`${ticketName}${separator}${ticketPrevenda}${separator}${ticketUniqueNumber}${separator}${lotType}${separator}${lotNumber}${separator}${lotUniqueNumber}${separator}${lotPrice}${separator}${lotPriceTax}`];


    //HERE IS THE FUNCTION THAT INCREMENT/DECREMENT ITEMS
    getSelectedQuantity = (ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax, myLotQuantity) => {
        // console.log(myLotQuantity);
        // console.log(this.state.selectedQuantities);
        // lot.reduce(function(prevVal, elem) {
        //     // const soma = parseInt(elem) + parseInt(lotQuantity);
        //     console.log(elem)
        //     // return soma;
        // }, 0);
        // console.log(myLotQuantity);
        // return myLotQuantity;
        
        
        // console.log(this.state.selectedQuantities);
        return this.state.selectedQuantities[
            `${ticketName}${separator}${ticketPrevenda}${separator}${ticketUniqueNumber}${separator}${lotType}${separator}${lotNumber}${separator}${lotUniqueNumber}${separator}${lotPrice}${separator}${lotPriceTax}${separator}${myLotQuantity}`
        ];
        // return lotQuantity;
    }

    // getAdditionEnabled = () => Object.values(this.state.selectedQuantities).reduce((acc, i) => acc + i, 0) < this.state.maxTotalItems;
    getAdditionEnabled = () => {
        var shouldShowButton = Object.values(this.state.selectedQuantities).reduce(function(acc, i) {
            const soma = parseInt(acc + i);
            return soma;
        }, 0);
        if(shouldShowButton < this.state.maxTotalItems){
            return true;
        } else{
            return false;
        }
    };

    onCheckoutButtonClick = () => {
        const selectedArray = Object.entries(this.state.selectedQuantities).map(
            ([key, quantity]) => {
                const [ticketName, ticketPrevenda, ticketUniqueNumber, lotType, lotNumber, lotUniqueNumber, lotPrice, lotPriceTax] = key.split(separator)
                const totalLotPrice = parseFloat(lotPrice + lotPriceTax);
                const total = parseFloat(totalLotPrice * quantity);
                return {
                    ticketName,
                    ticketPrevenda,
                    ticketUniqueNumber,
                    lotType,
                    lotNumber,
                    lotUniqueNumber,
                    lotPrice,
                    lotPriceTax,
                    quantity,
                    totalLotPrice,
                    total
                }
            },
        )
        console.log(selectedArray);
    }

    render() {
        return (
            <div>
                <Tabbar />
                <Header Title="Carrinho" ToPage="/" />
                <div className="cart">
                    <div className="container-fluid">
                        <div className="box-price">
                            <div className="row box-default ">
                                <div className="col col-price">
                                    <h6>{this.state.quantity} INGRESSO{this.state.quantity > 1 ? 'S' : ''}</h6>
                                    <h5>R$ {parseFloat(this.state.priceTotal).toFixed(2).replace('.', ',')}</h5>
                                </div>
                            </div>
                        </div>

                        <div className="box-default">
                            <div className="row no-margin">
                                <div className="col-12 col-image no-padding">
                                    <img src={this.state.banner_app} alt="" />
                                </div>
                                <div className="col-12 no-padding">
                                    <h5 className="event-name">{this.state.event_name}</h5>
                                </div>
                            </div>
                            {
                                this.state.lot.map((lot, l) =>
                                    // <div className="row" key={l}>
                                    //     <div className="col-8">
                                    //         <h5 className="lot-name">{lot.name}</h5>
                                    //         <h5 className="lot-name">
                                    //             {
                                    //                 lot.lotType === 'U' ? 'Unissex ' : ''
                                    //             }
                                    //             {
                                    //                 lot.lotType === 'M' ? 'Masculino ' : ''
                                    //             }
                                    //             {
                                    //                 lot.lotType === 'F' ? 'Feminino ' : ''
                                    //             }
                                    //             ({lot.lotNumber}º Lote)
                                    //         </h5>
                                    //         <h6 className="lot-price">
                                    //             R$ {lot.lotPrice.replace('.', ',')} <br />
                                    //             <small>(R$ {lot.lotPrice.replace('.', ',')} + R$ {lot.lotPriceTax.replace('.', ',')})</small>
                                    //         </h6>
                                    //     </div>
                                    //     <div className="col-4">
                                    //         <ChooseQuantity 
                                    //             value={this.getSelectedQuantity(lot.quantity)}
                                    //             onChange={this.onQuantityChange}
                                    //             additionEnabled={this.additionEnabled}
                                    //             maxPurchase={this.state.events.max_purchase}
                                    //             lotPrice={lot.lotPrice}
                                    //             lotPriceTax={lot.lotPriceTax}
                                    //             lotQuantity={lot.lotQuantity}
                                    //         />
                                    //     </div>
                                    //     <div className="col-12">
                                    //         {this.state.lot.length > 1 ? <hr /> : ''}
                                    //     </div>
                                    // </div>
                                    <div key={l}>
                                        <Lot
                                            events={this.state.events}
                                            ticketName={this.state.cart.events.tickets.name}
                                            ticketPrevenda={this.state.cart.events.tickets.prevenda}
                                            ticketUniqueNumber={this.state.cart.events.tickets.unique_number}
                                            lotUniqueNumber={lot.lotUniqueNumber}
                                            lotName={lot.name}
                                            lotType={lot.lotType}
                                            lotNumber={lot.lotNumber}
                                            lotPrice={lot.lotPrice}
                                            lotPriceTax={lot.lotPriceTax}
                                            onQuantityChange={this.onQuantityChange}
                                            maxPurchase={this.state.events.max_purchase}
                                            lotQuantity={this.getSelectedQuantity(this.state.cart.events.tickets.name, this.state.cart.events.tickets.prevenda, this.state.cart.events.tickets.unique_number, lot.lotType, lot.lotNumber, lot.lotUniqueNumber, lot.lotPrice, lot.lotPriceTax, lot.quantity)}
                                            // quantity={lot.quantity}
                                            additionEnabled={this.getAdditionEnabled()}
                                            // additionEnabled={this.state.showButton}
                                            myLotQuantity={lot.quantity}
                                        />
                                        {this.state.lot.length > 1 ? <hr /> : ''}
                                    </div>
                                )
                            }
                            {/* <div className="row cart-footer" style={{ marginRight: '-15px', marginLeft: '-15px', backgroundColor: '#f4f7fa' }}>
                                <Link to="/checkout" className="col col-purchase" style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    Confirmar Comprar
                                </Link>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Cart;