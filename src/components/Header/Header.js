import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

const url = window.location.pathname;
var link = true;
if(url === '/'){
    link = false;
}


class Header extends Component {
    constructor(props){
        super(props);
        this.state = {
            qtd: 0
        }
    }
    componentDidMount() {
        const qtd = JSON.parse(localStorage.getItem('qtd'));
        this.setState({qtd: qtd})
    }
    render() {
        const { Title, ToPage } = this.props;
        return (
            <div>
                <nav className="navbar">                
                    { link ? <Link to={ ToPage }><i className="fas fa-chevron-left"></i></Link> : null }
                    <Link to="/cart" className="icon-cart">
                        <i className="fas fa-shopping-cart"></i>
                        <span className="badge badge-danger">
                            {this.state.qtd}
                        </span>
                    </Link>
                    <div className="navbar-brand">
                        {Title}
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;