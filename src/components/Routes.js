import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Event from './Event/Event';

export class Routes extends Component {
    render() {
        return (
            <main className="container">
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact={true} component={App} />
                        <Route path="/event/:id" component={Event} />
                    </Switch>
                </BrowserRouter>
            </main>
        )
    }
};

export default Routes;