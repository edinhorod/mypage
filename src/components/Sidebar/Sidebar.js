import React from 'react';
import { Link } from 'react-router-dom';

import { slide as Menu } from 'react-burger-menu';
import './Sidebar.css';

export default props => {
  return (
    <Menu>
      <Link className="menu-item" to="/">Home</Link>
    </Menu>
  );
};