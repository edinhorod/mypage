import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

const Routes = () =>(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route path="/event/:id" component={Event} />
            <Route path="/menu" component={Menu} />
            <Route path="/my-tickets" component={MyTickets} />
            <Route path="/faq" component={Faq} />
        </Switch>
    </BrowserRouter>
);

export default Routes;