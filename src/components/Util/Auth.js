// export const isAuthenticated = () => false;
export const isAuthenticated = () => {
    let profile = localStorage.getItem('user_info');    
    if(profile === null){
        return false;
    } else{
        return true;
    }
};