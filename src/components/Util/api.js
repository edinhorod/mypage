import axios from "axios";
import { isAuthenticated } from '../../components/Util/Auth';
export const token  = 'Z31XC52XC4';

if (isAuthenticated()) {
    const user_info = JSON.parse(localStorage.getItem('user_info'));
    var auth_token = user_info.auth_token;    
}

export default axios.create({
    baseURL: 'https://web-services.yeapps.com.br/api/',
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Access-Origin': 'D',
        'Authorization': `Bearer ${auth_token}`,
        'Company-Token': 'Z31XC52XC4'
    }
});