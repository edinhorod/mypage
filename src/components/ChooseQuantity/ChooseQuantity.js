import React from 'react';
import './ChooseQuantity.css';

const ChooseQuantity = ({ value, onChange, additionEnabled }) => {
    const shouldIncrement = additionEnabled;
    const shouldDecrement = value > 0;
    
    const decrement = () => {
        if (shouldDecrement) {
            onChange(value - 1);
        }
    }
    
    const increment = () => {
        if (shouldIncrement) {
            onChange(value + 1);
        }
    }

    const decrementButton = shouldDecrement ? (
        <button className="minus" onClick={decrement}>
            <i className="fas fa-minus"></i>
        </button>
    ) : <div className="space-button"></div>

    const incrementButton = shouldIncrement ? (
        <button className='plus' onClick={increment}>
            <i className="fas fa-plus"></i>
        </button>
    ) : <div className="space-button"></div>

    return (
        <div>
            {decrementButton}
                <span className="qtd">{value}</span>
            {incrementButton}
        </div>
    )
}

ChooseQuantity.defaultProps = {
    value: 0,
    additionEnabled: true,
}

export default ChooseQuantity