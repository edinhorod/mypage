import React, { Component } from 'react';
import './ChooseQuantity.css';

const ChooseQuantity = ({ value, onChange, additionEnabled, onClick }) => {
    const shouldIncrement = additionEnabled;
    const shouldDecrement = value > 0;
    
    const decrement = () => {
        if (shouldDecrement) {
            onChange(value - 1);
        }
    }
    
    const increment = () => {
        if (shouldIncrement) {
            onChange(value + 1);
        }
    }

    const decrementButton = shouldDecrement ? (
        <button className="minus" onClick={decrement}>
            <i className="fas fa-minus"></i>
        </button>
    ) : <div className="space-button"></div>

    const incrementButton = shouldIncrement ? (
        <button className='plus' onClick={increment}>
            <i className="fas fa-plus"></i>
        </button>
    ) : <div className="space-button"></div>

    return (
        <div>
            {decrementButton}
                <span className="qtd">{value}</span>
            {incrementButton}
        </div>
    )
}

ChooseQuantity.defaultProps = {
    value: 0,
    additionEnabled: true,
}


export default ChooseQuantity

// var cart = {};
// var total = {};
// var events = {};
// var tickets = [];
// var quantity = [];
// class ChooseQuantity extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             qtd: 0,
//             showAddButton: true,
//             showRemoveButton: true,
//         }
//     }
//     addItem = (e) =>  {
//         localStorage.removeItem('quantity');

//         this.setState({ qtd: this.state.qtd + 1 });
//         this.setState({ showRemoveButton: true });

//         if (this.state.qtd + 1 >= this.props.max_purchase) {
//             this.setState({ showAddButton: false });
//         }

//         // quantity.push(parseInt(this.state.qtd));
//         // localStorage.setItem('quantity', quantity.length);
//         // console.log(this.state.showAddButton);
//         // console.log(this.state.qtd + 1)

//         if (tickets.indexOf(this.props.tickets)) {
//             tickets.push(this.props.tickets);
//         }

//         var price = parseFloat(this.props.price) * (this.state.qtd + 1);
//         total = { 
//             price: price, 
//             quantity: this.state.qtd + 1
//         };
//         // console.log(total);

//         events = {
//             banner_app: this.props.banner_app,
//             installments: this.props.installments,
//             max_purchase: this.props.max_purchase,
//             name: this.props.event_name,
//             tickets: tickets,
//             unique_number: this.props.unique_number
//         };
//         cart = { events: events, total: total };
//         // console.log(cart);
//         localStorage.setItem('cart', JSON.stringify(cart));
//         // console.log(localStorage.getItem('quantity'));
//         // if (localStorage.getItem(`${this.props.unique_number}-quantity`) >= this.props.max_purchase) {
//         //     this.setState({ showAddButton: false });
//         // }

//     }
//     removeItem = () => {
//         this.setState({ showAddButton: true });
//         if (this.state.qtd > 0) {
//             this.setState({ qtd: this.state.qtd - 1 });
//         }

//         if (this.state.qtd -1 == 0) {
//             this.setState({ showRemoveButton: false });
//         }

//         if (tickets.indexOf(this.props.tickets)) {
//             tickets.push(this.props.tickets);
//         }

//         var price = parseFloat(this.props.price) * (this.state.qtd - 1);
//         total = { 
//             price: price, 
//             quantity: this.state.qtd - 1
//         };
//         // console.log(total);

//         events = {
//             banner_app: this.props.banner_app,
//             installments: this.props.installments,
//             max_purchase: this.props.max_purchase,
//             name: this.props.event_name,
//             tickets: tickets,
//             unique_number: this.props.unique_number
//         };
//         cart = { events: events, total: total };
//         // console.log(cart);
//         localStorage.setItem('cart', JSON.stringify(cart));

//         if (this.state.qtd === 0) {
//             quantity = [];
//             quantity.pop();
//             quantity.length = '';
//             // localStorage.removeItem('quantity');
//             this.setState({ showRemoveButton: true });
//         } else{
//             this.setState({ showAddButton: true });
//             localStorage.setItem('qtd', this.state.qtd);
//             quantity.push(parseInt(this.state.qtd));
//             // quantity = JSON.parse(localStorage.getItem('quantity'));
//             localStorage.setItem('quantity', this.state.qtd);
//         }

//         console.log(localStorage.getItem('quantity'));
//         if(localStorage.getItem('quantity') >= this.props.max_purchase){
//             this.setState({ showRemoveButton: false });
//         }        

//         cart = JSON.parse(localStorage.getItem('cart'));

//         if (cart.events.tickets.indexOf(this.props.tickets)) {
//             localStorage.removeItem('cart');
//             console.log('retirou');
//             var price = this.props.price * this.state.qtd;

//             total = { price: price, quantity: this.state.qtd };
//             events = {
//                 banner_app: this.props.banner_app,
//                 installments: this.props.installments,
//                 max_purchase: this.props.max_purchase,
//                 name: this.props.event_name,
//                 tickets: tickets
//             };
//             cart = { events: events, total: total };
//             localStorage.setItem('cart', JSON.stringify(cart));
//             console.log(cart.events.tickets.indexOf(this.props.tickets));
//         }
//         // console.log(cart.events.tickets);
//         console.log(this.state.showAddButton);
//     }

//     handleChange = (e) => {
//         console.log(e.target.value)
//       }
//     componentDidMount() {

//     }

//     render() {
//         return (
//             <div>
//                 {
//                     this.state.showRemoveButton ?
//                         <button className="minus" onClick={() => this.removeItem()}>
//                             <i className="fas fa-minus"></i>
//                         </button>
//                     :
//                         <div className="space-button"></div>
//                 }
//                         <input className="qtd" type="text" value={this.state.qtd} name='qtd' onChange={(e) => {this.handleChange(e)}}  />
//                 {
//                     this.state.showAddButton ?
//                         <button className='plus' onClick={() => this.addItem()} >
//                             <i className="fas fa-plus"></i>
//                         </button>
//                     : 
//                         <div className="space-button"></div>
//                 }
//             </div>
//         )
//     }
// }

// export default ChooseQuantity;