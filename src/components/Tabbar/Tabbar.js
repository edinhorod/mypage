import React from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../components/Util/Auth';

import './Tabbar.css';

export default props => {
    return (
        <div className="row tabbar">
            <div className="col">
                <Link className="menu-item" to="/">
                    <i className="fas fa-glass-cheers"></i>
                    Events
                </Link>
            </div>
            <div className="col">
                <Link className="menu-item" to="/cart">
                    <i className="fas fa-glass-cheers"></i>
                    Cart
                </Link>
            </div>
            <div className="col">
                <Link className="menu-item" to="/">
                    <i className="fas fa-glass-cheers"></i>
                    Test
                </Link>
            </div>
            <div className="col">
                <Link className="menu-item" to="/">
                    <i className="fas fa-glass-cheers"></i>
                    Test
                </Link>
            </div>
        </div>
    );
};