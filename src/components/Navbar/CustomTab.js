import React from "react"

const style = {
  padding: "10px 0",
  borderBottom: "3px solid transparent",
  display: "inline-block",
  cursor: "pointer",
  backgroundColor: "#1c90ec",
  width: "33.3%",
  color: "rgba(255, 255, 255, .7)",
  textAlign: "center",
}

const teste = {
    // position: 'absolute',
    zIndex: '9999',
    // width: '100%',
    display: 'flex',
    justifyContent: 'center',
}

const activeStyle = {
  ...style,
  color: "white",
  borderBottom: "3px solid #d71356"
}

const CustomTab = ({ children, isActive }) => (
    <div className="row">
        <div className="col" style={teste}>
            <span style={isActive ? activeStyle : style}>{children}</span>
        </div>
    </div>
)

export default CustomTab