import React from 'react'
import { Tabs, TabList, TabPanel, Tab } from 'react-re-super-tabs'
import CustomTab from './CustomTab'
import Event from '../../pages/Event/Event';
import Home from '../../pages/Home/Home';

const Navbar = () => (
    <div>
        <Tabs activeTab='about' className="teste">
            <TabList>
                <Tab component={CustomTab} label='Home' id='home' />
                <Tab component={CustomTab} label='Event' id='event' />
            </TabList>
            <TabList>
                <TabPanel component={Home} id='home' />
                <TabPanel component={Event} id='event' />
            </TabList>
        </Tabs>
    </div>
)

export default Navbar