import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { isAuthenticated } from './components/Util/Auth';
import api, {token} from '../src/components/Util/api.js';//para requisições

import './index.css';
// Pages
import Home from './pages/Home/Home';
import Event from './pages/Event/Event';
import Menu from './pages/Menu/Menu';

// isAuthenticated
import Cart from './pages/Cart/Cart';
import Checkout from './pages/Checkout/Checkout';
// Pages

import registerServiceWorker from './registerServiceWorker';

api.get('users/company/' + token)
.then(res => {
    localStorage.setItem('company_info', JSON.stringify(res.data.data));
})

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            isAuthenticated() ? (
                <Component {...props} />
            ) : (
                    <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
                )
        }
    />
)

ReactDOM.render(
    
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route path="/event/:id" component={Event} />
            <Route path="/menu" component={Menu} />
            <Route path="/cart" component={Cart} />
            <Route path="/checkout" component={Checkout} />
        </Switch>
    </BrowserRouter>
    , document.getElementById('root'));
registerServiceWorker();
